# 使い方
- 赤地の内容  
コマンドプロンプト、ターミナルで実行するコマンド  

- 黒地の内容  
任意のファイルに対する書き込み内容  

## 初回起動時の設定手順  
前提 : python3.6系の実行環境
  
1. venv環境を作る  
`python -m venv {環境を作りたいフォルダのパス}`  
  
2. 作った環境にコードを置く(zipを解凍、git clone、その他いろいろ)

3. 作った環境をactivateする  
コマンドプロンプトの場合 `.\Scripts\activate.bat`  
Bashの場合 `source ./Scripts/activate`  

4. 作った環境にモジュールを入れる  
`pip install -r requirements.txt`  

5. 設定ファイルを作る  
`touch keyring.ini`  

6. 設定内容を書き込む
    ```
    [TOKENS]
    DISCORD_BOT = 取得したdiscord botアカウントのトークン
    ```
    ※ "取得したdiscord botアカウントのトークン" の部分は適宜、書き換える

7. botを起動する  
`python run.py`  

8. botを停止する (Ctrl + C)  

9. 作った環境をdeactivateする  
`deactivate`

## 2回目以降に起動・停止する場合
1. 作った環境をactivateする  

2. botを起動する  

3. botを停止する (Ctrl + C)  

4. 作った環境をdeactivateする  