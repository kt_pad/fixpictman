import discord # インストールした discord.py
import configparser
import requests

client = discord.Client() # 接続に使用するオブジェクト
config = configparser.ConfigParser() # 設定ファイルの読み書きに使用するオブジェクト
config.sections()
config.read('keyring.ini')

BOT_TOKEN = config['TOKENS']['DISCORD_BOT']

# 起動時
@client.event
async def on_ready():
    print('start!')

@client.event
async def on_message(message):
    if message.content.startswith('/shitaraba'):
        user_send_message = message.content.split(' ')
        command = user_send_message[1]

        if command == 't':
            category = user_send_message[2]
            board_no = user_send_message[3]
            response = requests.get('https://jbbs.shitaraba.net/' + category + '/' + board_no + '/subject.txt')
            response.encoding = response.apparent_encoding

        await message.channel.send(response.text)

# botの接続と起動.
client.run(BOT_TOKEN)
